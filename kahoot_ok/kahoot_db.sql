-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 06-05-2019 a las 20:30:38
-- Versión del servidor: 5.7.26-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.17-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `kahoot_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190505145025', '2019-05-05 14:53:29'),
('20190506095231', '2019-05-06 09:52:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntes`
--

CREATE TABLE `preguntes` (
  `id` int(11) NOT NULL,
  `pregunta` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `categoria` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `preguntes`
--

INSERT INTO `preguntes` (`id`, `pregunta`, `categoria`) VALUES
(1, 'Completa la frase de Han Solo: \"No hay recompensa que...', 'cine'),
(2, '¿Cuál es la última película que dirigió Sergio Leone?', 'cine'),
(3, '¿Qué es un polinomio?', 'mates'),
(4, '¿Qué es un Dominio?', 'mates'),
(5, '¿Quienes fueron los creadores del Internet?', 'tecnologia'),
(6, '¿Quien creo la primer computadora?', 'tecnologia'),
(7, '¿Qué es Zelandia?', 'geografia'),
(8, '¿Cuál es el país de los 1.000 lagos?', 'geografia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respostes`
--

CREATE TABLE `respostes` (
  `id` int(11) NOT NULL,
  `id_pregunta` int(11) NOT NULL,
  `resposta` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `correcta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `respostes`
--

INSERT INTO `respostes` (`id`, `id_pregunta`, `resposta`, `correcta`) VALUES
(1, 1, 'valga la pena si mueres', 0),
(3, 1, 'compense esto', 1),
(4, 1, 'compense soportarte', 0),
(7, 1, 'que me saque de pobre', 0),
(8, 2, 'Érase una vez América', 1),
(9, 2, 'El bueno, el feo y el malo', 0),
(10, 2, 'Hasta que llegó su hora', 0),
(11, 2, 'Adiós mamá', 0),
(12, 3, 'Una ecuación canónica. ', 0),
(13, 3, 'Una ecuación irracional. ', 0),
(14, 3, 'Una ecuación.', 0),
(15, 3, 'Es la suma-resta de un conjunto de monomios.', 1),
(16, 4, 'Someter a una persona a tu voluntad. ', 0),
(17, 4, 'Tener posesión de un objeto.', 0),
(18, 4, 'Conjunto de valores de la variable independiente para los cuales no existe la función.', 0),
(19, 4, 'Conjunto de valores de la variable independiente para los cuales existe la función. ', 1),
(20, 5, 'Robert kahn/ Vinton Cerf ', 1),
(21, 5, 'Bill Gates/ Steve Jobs ', 0),
(22, 5, 'Charles Babbage/ Howard H. Aiken ', 0),
(23, 5, 'Tomas Hooped / Adams Benson', 0),
(24, 6, 'Bill Gates ', 0),
(25, 6, 'Charles Babbage ', 1),
(26, 6, 'Steve Jobs', 0),
(27, 6, 'Charles Babbage/ Howard H. Aiken ', 0),
(28, 7, ' Un parque temático ', 0),
(29, 7, 'Un país del hemisferio norte', 0),
(30, 7, 'Un continente', 1),
(31, 7, 'Una ciudad islandesa', 0),
(32, 8, 'Finlandia', 1),
(33, 8, 'Estados Unidos', 0),
(34, 8, ' Canadá', 0),
(35, 8, 'Brasil', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `preguntes`
--
ALTER TABLE `preguntes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `respostes`
--
ALTER TABLE `respostes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pregunta` (`id_pregunta`),
  ADD KEY `id_pregunta_2` (`id_pregunta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `preguntes`
--
ALTER TABLE `preguntes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `respostes`
--
ALTER TABLE `respostes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `respostes`
--
ALTER TABLE `respostes`
  ADD CONSTRAINT `respostes_ibfk_1` FOREIGN KEY (`id_pregunta`) REFERENCES `preguntes` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
