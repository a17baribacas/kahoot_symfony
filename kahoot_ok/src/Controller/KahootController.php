<?php


namespace App\Controller;

use App\Entity\Preguntes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Respostes;
use App\Form\KahootForm;


class KahootController extends FOSRestController

{
    /**
     * Lists all Preguntes.
     * @Rest\Get("/categoria/{categoria}")
     *
     * @return Response
     */
    public function getCategoria(Request $request, $categoria)
    {
        $preguntes = $this->getDoctrine()->getRepository(Preguntes::class)
            ->findBy(['categoria' => $categoria]);


        $random = rand(0, sizeof($preguntes)-1);

        $pregunta = $preguntes[1]->getRespostes();

        return $this->handleView($this->view($pregunta));
    }


    /**
     * Listar una pregunta por categoria, de forma aleatoria
     * @Rest\Get("/mostrar_pregunta_cat/{categoria}")
     *
     * @return Response
     * @throws \Exception
     *
    public function preguntaCategoria(Request $request, $categoria)
    {
        $serializer = $this->get('jms_serializer');
        $repository = $this->getDoctrine()->getManager();
        $preguntas = [];

        try {
            $code = 200;
            $error = false;

            $preguntas_cat = $categoria;
            $preguntas = $repository->getRepository(Preguntes::class)->find($preguntas_cat);

            if (is_null($preguntas)) {
                $code = 500;
                $error = true;
                $message = "La pregunta no existe";
            }

        } catch (Exception $ex) {
            $code = 500;
            $error = true;
            $message = "Ha ocurrido un error intentando encontrar la pregunta - Error: {$ex->getMessage()}";
        }

        $preguntas = [
            'code' => $code,
            'error' => $error,
            'data' => $code == 200 ? $preguntas : $message,
        ];

        $totalPreguntas = $this->getDoctrine()
            ->getRepository(Preguntes::class)
            ->countNumberPrintedForCategory($categoria);

        var_dump($totalPreguntas);

        $random = random_int(1, $totalPreguntas);

        $conn = $this->getEntityManager()->getConnection();
        $query = 'SELECT pregunta FROM preguntes WHERE
                         id = :random and :preguntas_cat = categoria';

        $preguntas = $conn-> prepare($query);
        $preguntas-> execute(['random' => $random], ['preguntas_cat' => $preguntas_cat] );
        $preguntas->fetchAll();

        return new Response($serializer->serialize($preguntas, "json"));
    }*/
}