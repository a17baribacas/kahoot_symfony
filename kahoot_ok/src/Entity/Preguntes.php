<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PreguntesRepository")
 */
class Preguntes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $pregunta;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $categoria;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPregunta(): ?string
    {
        return $this->pregunta;
    }

    public function setPregunta(string $pregunta): self
    {
        $this->pregunta = $pregunta;

        return $this;
    }

    public function getCategoria(): ?string
    {
        return $this->categoria;
    }

    public function setCategoria(string $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * @return Collection|Respostes[]
     */
    public function getRespostes(): Collection
    {
        return $this->respostes;
    }

    public function addResposte(Respostes $resposta): self
    {
        if (!$this->respostes->contains($resposta)) {
            $this->respostes[] = $resposta;
            $resposta->setPregunta($this);
        }

        return $this;
    }

    public function removeResposte(Respostes $resposta): self
    {
        if ($this->respostes->contains($resposta)) {
            $this->respostes->removeElement($resposta);
            // set the owning side to null (unless already changed)
            if ($resposta->getPregunta() === $this) {
                $resposta->setPregunta(null);
            }
        }

        return $this;
    }
}
