<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RespostesRepository")
 */
class Respostes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_pregunta;

    /**
     * @ORM\Column(type="text")
     */
    private $resposta;

    /**
     * @ORM\Column(type="integer")
     */
    private $correcta;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdPregunta(): ?int
    {
        return $this->id_pregunta;
    }

    public function setIdPregunta(int $id_pregunta): self
    {
        $this->id_pregunta = $id_pregunta;

        return $this;
    }

    public function getResposta(): ?string
    {
        return $this->resposta;
    }

    public function setResposta(string $resposta): self
    {
        $this->resposta = $resposta;

        return $this;
    }

    public function getCorrecta(): ?int
    {
        return $this->correcta;
    }

    public function setCorrecta(int $correcta): self
    {
        $this->correcta = $correcta;

        return $this;
    }
}
