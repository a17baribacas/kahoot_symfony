<?php

namespace App\Repository;

use App\Entity\Respostes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Respostes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Respostes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Respostes[]    findAll()
 * @method Respostes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RespostesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Respostes::class);
    }

    // /**
    //  * @return Respostes[] Returns an array of Respostes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Respostes
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
