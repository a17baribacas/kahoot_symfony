<?php

namespace App\Repository;

use App\Entity\Preguntes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Preguntes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Preguntes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Preguntes[]    findAll()
 * @method Preguntes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PreguntesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Preguntes::class);
    }

    // /**
    //  * @return Preguntes[] Returns an array of Preguntes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Preguntes
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
